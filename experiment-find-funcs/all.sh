function find_funcs() {
	pattern="$1"
	declare -F | grep $pattern | sed 's/declare -f //g' # | sed -E 's/^(.*)$/"\1"/g'
}
source test1.sh
source test2.sh
source test3.sh

for fn in $(find_funcs "test_"); do
	echo "Loading $fn .."
	$fn
done
