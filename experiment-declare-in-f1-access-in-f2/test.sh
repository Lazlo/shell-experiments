#!/bin/bash

set -e
set -u

function step1() {
	echo "Declare var"
	x=52
}

function step2() {
	echo "Access var"
	echo $x
}

step1
step2
