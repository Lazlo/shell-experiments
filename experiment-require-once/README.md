# require once in bash

This experiment shows how to include a script only once, even though many sub-scripts might source the script multiple times.
